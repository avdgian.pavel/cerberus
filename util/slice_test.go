package util

import "testing"

func TestSliceContains(t *testing.T) {
	sl := []string{"test", "one", "two"}

	if !SliceContains(sl, "test") {
		t.Log("expected to find value in a slice")
		t.Fail()
	}
	if SliceContains(sl, "notthere") {
		t.Log("expected SliceContains to return false")
		t.Fail()
	}
}
