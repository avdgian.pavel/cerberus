package util

func SliceContains[T string | int](sl []T, search T) bool {
	for _, v := range sl {
		if v == search {
			return true
		}
	}

	return false
}
