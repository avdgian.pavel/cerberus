FROM golang:1.18-alpine AS builder

WORKDIR /go/src/gitlab.com/avdgian.pavel/cerberus/

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY ./ ./
RUN go build ./cmd/cerberus.go

FROM alpine

WORKDIR /root/

COPY --from=builder /go/src/gitlab.com/avdgian.pavel/cerberus/cerberus ./
COPY --from=builder /go/src/gitlab.com/avdgian.pavel/cerberus/.env ./

EXPOSE $PORT
CMD ["./cerberus"]
