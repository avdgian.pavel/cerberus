package cerberus

import (
	"context"
	"errors"
	"fmt"

	"github.com/go-kit/kit/endpoint"
	httptransport "github.com/go-kit/kit/transport/http"
)

type contextKey string

const HostKey contextKey = "Host"

type ClientCredentialsResponse struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
}

type AuthenticatedClientResponse struct {
	ClientId string `json:"client_id"`
	Domain   string `json:"domain"`
}

func MakeCredentialsEndpoint(service ClientService) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		host := ctx.Value(httptransport.ContextKeyRequestHost).(string)

		c, err := service.CreateClient(host)
		if err != nil {
			return nil, fmt.Errorf("error generating client credentials: %w", err)
		}

		return ClientCredentialsResponse{
			ClientId:     c.GetID(),
			ClientSecret: c.GetSecret(),
		}, nil
	}
}

func MakeClientEndpoint(service ClientService) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		cl, valid := ctx.Value(AuthenticatedClientKey).(*Client)
		if !valid {
			return nil, errors.New("no client in context")
		}
		return AuthenticatedClientResponse{
			ClientId: cl.GetID(),
			Domain:   cl.GetDomain(),
		}, nil
	}
}
