package oauth

import (
	"gitlab.com/avdgian.pavel/cerberus"
	"gopkg.in/oauth2.v3"
)

type ClientStore struct {
	repo cerberus.ClientRepository
}

func NewClientStore(repo cerberus.ClientRepository) *ClientStore {
	return &ClientStore{
		repo,
	}
}

// according to the ID for the client information
func (st *ClientStore) GetByID(id string) (oauth2.ClientInfo, error) {
	client, err := st.repo.GetByID(id)
	if err != nil {
		return nil, err
	}

	return client, nil
}
