package cerberus

import "fmt"

type ClientService interface {
	CreateClient(domain string) (*Client, error)
	GetClient(id string) (*Client, error)
}

type clientService struct {
	repo ClientRepository
}

func NewClientService(repo ClientRepository) ClientService {
	return &clientService{
		repo,
	}
}

func (s *clientService) CreateClient(domain string) (*Client, error) {
	client := NewClient(domain)

	if err := s.repo.Create(client); err != nil {
		return nil, fmt.Errorf("error creating client: %w", err)
	}

	return client, nil
}

func (s *clientService) GetClient(id string) (*Client, error) {
	return s.repo.GetByID(id)
}
