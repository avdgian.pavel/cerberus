### Build and run the app
- `cp .env.example .env`
- `docker build . -t cerberus`
- `docker run -p9096:9096 --env-file=.env --name=cerberus cerberus`

### Build and run tests
- `docker build -f=Dockerfile.test -t cerberus_test .`
- `docker run cerberus_test`

### Endpoints
- `/credentials` - generate client credentials
- `/oauth/token` - generate access token
- `/me` - protected endpoint, requires authentication
Take a look at Postman collection for more info
