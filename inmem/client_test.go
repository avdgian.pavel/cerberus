package inmem

import (
	"testing"

	"gitlab.com/avdgian.pavel/cerberus"
)

func TestCreateClient(t *testing.T) {
	repo := NewInMemoryClientRepository()
	client := cerberus.NewClient("test.test")
	err := repo.Create(client)
	if err != nil {
		t.Log("error saving client")
		t.Fail()
	}
}

func TestGetClientByID(t *testing.T) {
	repo := NewInMemoryClientRepository()
	client := cerberus.NewClient("test.test")
	repo.Create(client)

	cl, err := repo.GetByID(client.GetID())
	if err != nil {
		t.Logf("error GetByID not found with id %s\n", client.GetID())
		t.Fail()
	} else if cl == nil {
		t.Log("error GetByID empty client returned")
		t.Fail()
	} else if cl.GetID() != client.GetID() {
		t.Logf("client id mismatch. Expected: %s, got: %s\n", client.GetID(), cl.GetID())
		t.Fail()
	}
}

func TestClientNotFound(t *testing.T) {
	repo := NewInMemoryClientRepository()

	_, err := repo.GetByID("non-existent-id")
	if err == nil {
		t.Log("expected error")
	}
	if err != cerberus.ErrClientNotFound {
		t.Logf("expected error: %s, got: %s\n", cerberus.ErrClientNotFound.Error(), err.Error())
		t.Fail()
	}
}
