package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/avdgian.pavel/cerberus"
	"gitlab.com/avdgian.pavel/cerberus/basic"
	"gitlab.com/avdgian.pavel/cerberus/inmem"
	"gitlab.com/avdgian.pavel/cerberus/oauth"

	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/joho/godotenv"
	"gopkg.in/oauth2.v3/errors"
	"gopkg.in/oauth2.v3/manage"
	oauthserver "gopkg.in/oauth2.v3/server"
	"gopkg.in/oauth2.v3/store"
)

func init() {
	if err := godotenv.Load(".env"); err != nil {
		log.Fatalf("error loading .env file: %s\n", err.Error())
	}
}

func main() {
	clientRepo := inmem.NewInMemoryClientRepository()
	clientService := cerberus.NewClientService(clientRepo)

	manager := manage.NewDefaultManager()
	manager.SetAuthorizeCodeTokenCfg(manage.DefaultAuthorizeCodeTokenCfg)
	manager.MustTokenStorage(store.NewMemoryTokenStore())

	clientStore := oauth.NewClientStore(clientRepo)
	manager.MapClientStorage(clientStore)

	srv := oauthserver.NewDefaultServer(manager)
	srv.SetAllowGetAccessRequest(true)
	srv.SetClientInfoHandler(oauthserver.ClientFormHandler)
	manager.SetRefreshTokenCfg(manage.DefaultRefreshTokenCfg)

	srv.SetInternalErrorHandler(func(err error) (re *errors.Response) {
		log.Println("Internal Error:", err.Error())
		return
	})
	srv.SetResponseErrorHandler(func(re *errors.Response) {
		log.Println("Response Error:", re.Error.Error())
	})

	oauthHosts := strings.Split(os.Getenv("OAUTH_HOSTS"), ",")
	basicHosts := strings.Split(os.Getenv("BASIC_HOSTS"), ",")

	oauthAuthenticator := oauth.NewOauthAuthenticator(srv, clientService, oauthHosts)
	basicAuthenticator := basic.NewBasicAuthAuthenticator(clientService, basicHosts)
	authenticator := cerberus.NewAuthenticator([]cerberus.AuthenticationStrategy{
		oauthAuthenticator,
		basicAuthenticator,
	})
	tkMiddleware := cerberus.NewTokenValidator(func(tk cerberus.Token, host string) (*cerberus.Client, error) {
		return authenticator.Authenticate(tk, host)
	})

	contentType := httptransport.SetContentType("application/json")
	defaultOptions := []httptransport.ServerOption{
		httptransport.ServerBefore(func(ctx context.Context, req *http.Request) context.Context {
			return httptransport.PopulateRequestContext(ctx, req)
		}),
		httptransport.ServerAfter(contentType),
		httptransport.ServerErrorEncoder(encodeError),
	}

	credentialsTransport := httptransport.NewServer(
		cerberus.MakeCredentialsEndpoint(clientService),
		decodeEmptyRequest,
		encodeResponse,
		defaultOptions...,
	)
	clientTransport := httptransport.NewServer(
		tkMiddleware(cerberus.MakeClientEndpoint(clientService)),
		decodeEmptyRequest,
		encodeResponse,
		defaultOptions...,
	)

	http.HandleFunc("/oauth/token", func(w http.ResponseWriter, r *http.Request) {
		srv.HandleTokenRequest(w, r)
	})
	http.Handle("/credentials", credentialsTransport)
	http.Handle("/me", clientTransport)

	log.Printf("port is: %s", os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), nil))
}

func decodeEmptyRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	if response == cerberus.ErrUnauthorized {
		w.WriteHeader(http.StatusUnauthorized)
	}

	return json.NewEncoder(w).Encode(response)
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	msg := err.Error()
	code := http.StatusBadRequest
	if err == cerberus.ErrUnauthorized {
		code = http.StatusUnauthorized
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(map[string]string{"error": msg})
}
